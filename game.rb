# frozen_string_literal: true

class Game
  require_relative 'mastermind.rb'

  def initialize(game)
    setup(game)
  end

    public

  def play_game
    @game.play_next_turn until @game.game_over?
    @game.declare_winner
    rematch = play_again?
    if rematch
      setup(@game.to_s)
      play_game
    else
      puts 'Thanks for playing!'
    end
  end

    private

  def setup(game)
    case game
    when 'mastermind'
      @game = Mastermind.new
    else
      puts 'Error: Unknown Game!'
    end
  end

  def play_again?
    user_input = ''
    while user_input != 'Y' && user_input != 'N'
      puts 'Want to play again? [Y/N]'
      user_input = gets.chomp.upcase
    end
    user_input == 'Y'
  end
  end

my_game = Game.new('mastermind')
my_game.play_game
