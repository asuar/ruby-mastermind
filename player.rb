# frozen_string_literal: true

class Player
  def initialize(name, role)
    @name = name
    @codebreaker = role
  end

  def to_s
    @name
  end

  def codebreaker?
    @codebreaker
  end
end
