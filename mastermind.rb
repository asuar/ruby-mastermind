# frozen_string_literal: true

class Mastermind
  require_relative 'player.rb'
  require_relative 'computer_player.rb'
  @@WIN_MESSAGES = ['Foreign Nuclear Launch disabled!', 'Rogue Agent Bitcoin wallet accessed!',
                    'Incriminating documents destroyed!']
  @@LOSE_MESSAGES = ['A SWAT team has been deployed to your location!',
                     'A 3.2 million dollar bounty has been placed on your head!',
                     'Your location has been compromised!']

  def initialize
    show_starting_screen
    initialize_players
    initialize_game
    @turn_count = 0
    @previous_attempt = ''
  end

  public

  def to_s
    'mastermind'
  end

  def game_over?
    if @turn_count == @difficulty || @previous_attempt == @secret_code
      return true
    end

    false
  end

  def play_next_turn
    display_clue
    @turn_count += 1
    puts "Attempt #{@turn_count}!"
    attempt_code_decipher
    print @player1.codebreaker? ? 'Attempting to decipher' : 'Attempting to send transmission'
    display_waiting(1)
  end

  def declare_winner
    if game_over?
      win_message = (@@WIN_MESSAGES[rand(0..(@@WIN_MESSAGES.size - 1))]).to_s
      lose_message = (@@LOSE_MESSAGES[rand(0..(@@LOSE_MESSAGES.size - 1))]).to_s
      display_clue
      if @previous_attempt == @secret_code
        if @player1.codebreaker?
          puts "#{make_green('CodeBreaker')} #{make_green(@player1)} #{make_green('has cracked the code!')}"
          puts win_message
        else
          puts "#{make_red('CodeBreaker')} #{make_red(@computer_player)} #{make_red('has cracked the code!')}"
          puts lose_message
        end
      else
        if @player1.codebreaker?
          puts "#{make_red('CodeMaker')} #{make_red(@computer_player)} #{make_red('has made an uncrackable code!')}"
          puts lose_message
        else
          puts "#{make_green('CodeMaker')} #{make_green(@player1)} #{make_green('has made an uncrackable code!')}"
          puts win_message
        end
        end
    end
  end

  private

  def show_starting_screen
    puts '*******************************'
    puts "* #{make_red('Mastermind')} by #{make_green('Alain Suarez')}  *"
    puts '*******************************'
    puts '* Your mission, should you    *'
    puts '* choose to accept it:        *'
    puts '* CodeMaker: Create 4 number  *'
    puts '* sequence consisting of 1-6. *'
    puts '*******************************'
    puts '* CodeBreaker: Decipher the   *'
    puts '* secret number sequence.     *'
    puts '*******************************'
    puts '* A correct number in the     *'
    puts "* correct position: #{make_green('1')}         *"
    puts '* A correct number in the     *'
    puts '* wrong position: 2           *'
    puts "* A wrong number: #{make_red('3')}           *"
    puts '*******************************'
  end

  def initialize_players
    create_player
    player_breaker = @player1.codebreaker?
    create_computer_player(!player_breaker)
    display_game_start(player_breaker)
  end

  def create_player
    role = ''
    until role.match(/\bbreaker|\bmaker/i)
      puts 'Are you a CodeBreaker or a CodeMaker Agent? (Type BREAKER or MAKER)'
      role = gets.chomp.upcase
    end

    player_breaker = role.match(/\bbreaker/i) ? true : false
    puts "What is your name Code#{player_breaker ? 'Breaker' : 'Maker'}?"
    @player1 = Player.new(gets.chomp.capitalize, player_breaker)
  end

  def display_game_start(player_breaker)
    if player_breaker
      puts "Decipher target's secret code to complete your mission."
    else
      puts 'Create a secret code to prevent our transmission from being decrypted!'
    end

    sleep 1.5
    puts 'Advance Payment has been transferred to your account.'
    sleep 1
    print "#{make_green('Connecting')} to TOR Network"
    display_waiting(1.5)
    sleep 1

    if player_breaker
      puts "Your Target CodeMaker is #{make_red(@computer_player)}!"
    else
      puts "Beware CodeBreaker #{make_red(@computer_player)}!"
    end
    puts '*******************************'
    sleep 1.5
  end

  def colorize(text, color_code)
    "#{color_code}#{text}\e[0m"
  end

  def make_red(text)
    colorize(text, "\033[0;31m")
  end

  def make_green(text)
    colorize(text, "\033[0;32m")
  end

  def create_computer_player(computer_role)
    @computer_player = ComputerPlayer.new(computer_role)
  end

  def initialize_game
    @player1.codebreaker? ? get_difficulty : set_difficulty('4')
    create_secret_code
  end

  def get_difficulty
    puts 'What difficulty of code can you decipher?'
    sleep 0.5
    puts '1-Easy (12 attempts), 2-Medium (10 attempts), 3-Hard (8 attempts), 4-Extreme (6 attempts)'
    sleep 0.5
    @difficulty = '0'
    while @difficulty !~ /^[1-4]$/
      puts 'Enter 1-4 to choose:'
      @difficulty = gets.chomp
    end
    set_difficulty(@difficulty)
  end

  def set_difficulty(difficulty)
    case difficulty
    when '1'
      @difficulty = 12
    when '2'
      @difficulty = 10
    when '3'
      @difficulty = 8
    when '4'
      @difficulty = 6
    end
  end

  def create_secret_code
    if @player1.codebreaker?
      create_computer_secret_code
    else
      create_player_secret_code
    end
  end

  def create_player_secret_code
    @secret_code = ''
    while @secret_code !~ /^[1-6]{4}$/
      puts "Enter a number sequence (4 digits, 1-6) to use as encryption code Agent #{@player1}!"
      @secret_code = gets.chomp
    end
  end

  def create_computer_secret_code
    print "Waiting for #{make_red(@computer_player)} to exchange a code"
    display_waiting(2)
    print "Stealing Sequence from #{make_red('remote host')}"
    display_waiting(1.5)
    sleep 1
    puts make_green('Code transmission received!').to_s
    puts '*******************************'
    @secret_code = @computer_player.create_code
  end

  def display_clue
    puts "Hacking Result: #{get_clue_string}" if @previous_attempt != ''
  end

  def get_clue_string
    solution_numbers = check_previous_guess.split('')
    guess_numbers = @previous_attempt.split('')
    clue_string = ''
    guess_numbers.each_with_index do |number, index|
      case solution_numbers[index]
      when '2'
        clue_string += make_green(number)
      when '1'
        clue_string += number
      when '0'
        clue_string += make_red(number)
      end
    end
    clue_string
  end

  def attempt_code_decipher
    if @player1.codebreaker?
      @previous_attempt = ''
      while @previous_attempt !~ /^[1-6]{4}$/
        puts "Enter a number sequence (4 digits, 1-6) to crack the code Agent #{@player1}!"
        @previous_attempt = gets.chomp
      end
    else
      @previous_attempt = @computer_player.get_break_code_attempt(@previous_attempt, check_previous_guess)
    end
  end

  def display_waiting(time_to_wait)
    sleep time_to_wait
    print '.'
    sleep time_to_wait
    print '.'
    sleep time_to_wait
    puts '.'
  end

  def check_previous_guess
    if (@previous_attempt.nil? || @previous_attempt.empty?) || (@secret_code.nil? || @secret_code.empty?)
      return '0000'
    end

    guess_numbers = @previous_attempt.split('')
    solution_numbers = @secret_code.split('')
    correct_clues = ''
    guess_numbers.each_with_index do |number, index|
      correct_clues += if solution_numbers[index] == guess_numbers[index]
                         '2'
                       elsif solution_numbers.include? number
                         '1'
                       else
                         '0'
                       end
    end
    correct_clues
  end
end
