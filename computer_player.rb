# frozen_string_literal: true

class ComputerPlayer < Player
  require_relative 'player.rb'
  require 'set'

  def initialize(role)
    @name = "Agent 00#{rand((1..9))}"
    @codebreaker = role
    @attemped_sub_sequences = Hash.new{|hsh,key| hsh[key] = Set.new }
    @possible_numbers = %w[1 2 3 4 5 6]
    end

  public

  def create_code
    create_random_code_from_possible
  end

  def get_break_code_attempt(previous_attempt, previous_attempt_result)
    return create_random_code_from_possible if previous_attempt_result == '0000'
    previous_attempt_numbers = previous_attempt.split('')
    previous_attempt_individual_results = previous_attempt_result.split('')
    learn_from_previous(previous_attempt_individual_results, previous_attempt_numbers)
    return generate_new_code(previous_attempt_individual_results, previous_attempt_numbers)
  end

  private

  def create_random_code_from_possible
    "#{@possible_numbers.sample}#{@possible_numbers.sample}#{@possible_numbers.sample}#{@possible_numbers.sample}"
  end

  def match_sub_sequence(current_guess)
    current_guess_array = current_guess.split('')
    current_guess_array.each_with_index do |number, index|
      return true if @attemped_sub_sequences[number].include? index
    end
    false
  end

  def learn_from_previous(previous_attempt_individual_results,previous_attempt_numbers)
      previous_attempt_individual_results.each_with_index do |number, index|
        case number
        when '0'
          @possible_numbers -= [previous_attempt_numbers[index]] #remove from possible values because its not in sequence
        when '1'
          @attemped_sub_sequences[previous_attempt_numbers[index]].add index #store that we tried this number in this position already
        end
      end
  end
  
  def generate_new_code(previous_attempt_individual_results, previous_attempt_numbers)
    next_guess = ''
    while match_sub_sequence(next_guess) || next_guess == ""
      next_guess = ''
      previous_attempt_individual_results.each_with_index do |number, index|
        if number == '2'
          next_guess += previous_attempt_numbers[index]
        else
          next_guess += @possible_numbers.sample  
        end
      end
    end
    next_guess
  end

end
